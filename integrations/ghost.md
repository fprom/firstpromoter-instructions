# Adding FirstPromoter to your Ghost Website

The integration between FirstPromoter and Ghost is fully automated and does not require any additional scripts to be added.

## Setting things up

1. Click  [here to view the instructions](https://ghost.org/integrations/firstpromoter/).

### Test Click Tracking

@[trackingtest]("click")

### Test Referral Tracking

@[trackingtest]("referral")
