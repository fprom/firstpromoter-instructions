![Click Funnels](/images/click-funnels-logo.png/raw?ref=main)

# Add Click Funnels to your website

#### Some introduction here lorem ipsum dolor sit amet consectetur. Quis consectetur c

&nbsp;

## Main tracking script

### Substep title

replace `USER_ID` with a unique identifier, preferably the user ID from your database. Including the user's email is also suggested, along with any additional "traits" like avatar, name, role, etc. This information will appear on the user's profile and can be used to filter users in reports.

```javascript {noCopy}
console.log("no g'drou"p");
```

```javascript
console.log("no g'drou"p");
```

### Substep title

1. Lorem ipsum dolor sit amet consectetur. Nunc nunc non auctor lacinia. Lacus metus blandit adipiscing.
2. Nunc nunc non auctor lacinia. Lacus metus blandit adipiscing.

@[trackingtest]("click")

## Referral tracking script

### Substep title

replace `USER_ID` with a unique identifier, preferably the user ID from your database. Including the user's email is also suggested, along with any additional "traits" like avatar, name, role, etc. This information will appear on the user's profile and can be used to filter users in reports.

```js [g1:Javascript]
window.analytics.identify("USER_ID", {
  email: "test@example.com",
  // Optional
  name: "Joe Bloggs",
  avatar: "https://avatar.com/asd809sdhoif9as10nc29.png/raw?ref=main",
  // Add anything else about the user here
});
```

```ruby [g1:Ruby] {noCopy}
Ruby code here
```

```php [g1:PHP]
PHP code here
```

@[trackingtest]("referral")

### Unordered list sample

- Lorem ipsum dolor sit amet consectetur. Nunc nunc non auctor lacinia. Lacus metus blandit adipiscing.
- Nunc nunc non auctor lacinia. Lacus metus blandit adipiscing.
