# Connecting FirstPromoter with Stripe

FirstPromoter allows you to automatically track sales, refunds, upgrades and cancellations from Stripe.

## Setting Up

To get started, you need to follow the below steps:

1. Click on the "Connect Stripe" Button. This will take you to the Stripe marketplace, requesting you to install FirstPromoter.
2. Select your Stripe account and install the app.
3. After the installation is completed, you will be redirected back to this page to finish the setup.

## Switching between test and live mode on Stripe

To switch between test mode and live mode on Stripe you will need to **disconnect and reconnect** to the desired mode.

![stripe environment switch guide](https://gitlab.com/api/v4/projects/55002365/repository/files/images%2Fstripe-environment-switch-guide.gif/raw?ref=main)
